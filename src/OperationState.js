import React from "react";

export function OperationState({ loading, success, error }) {
  if (loading) {
    return "Loading...";
  }
  if (success) {
    return "Success";
  }
  if (error) {
    return <div style={{ color: "red" }}>{"Error " + error}</div>;
  }
  return null;
}
