import React, { useEffect, useReducer, useState } from "react";
import { Accordion } from "grommet";
import { TezosOperationError } from "@taquito/taquito";

import { useContract } from "@tezos-il/tezos-hooks";
import { TransferForm } from "./TransferForm";
import { ApproveForm } from "./ApproveForm";
import { StorageTab } from "./StorageTab";

const types = {
  OPERATION_STARTED: "OPERATION_STARTED",
  OPERATION_FINISHED: "OPERATION_FINISHED",
  OPERATION_FAILED: "OPERATION_FAILED",
  CLEAR: "CLEAR",
};

const operations = {
  TRANSFER: "TRANSFER",
  APPROVE: "APPROVE",
  GET_ALLOWANCE: "GET_ALLOWANCE",
  GET_BALANCE: "GET_BALANCE",
  GET_TOTAL_SUPPLY: "GET_TOTAL_SUPPLY",
};

export function ContractInterface({ address }) {
  const { contract, connect, ...contractState } = useContract(address);

  const [operationState, dispatch] = useReducer(
    stateReducer,
    Object.fromEntries(
      Object.values(operations).map((key) => [
        key,
        {
          loading: false,
          error: null,
        },
      ])
    )
  );

  const [storage, setStorage] = useState(null);

  useEffect(() => {
    connect(address);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [address]);

  useEffect(() => {
    fetchAndSetStorage(address);
  }, [address, contractState.operationsCount]);

  if (contractState.loading) {
    return "Loading...";
  }

  if (contractState.error) {
    return "Error: " + contractState.error;
  }

  if (!contract) {
    return "Choose a Contract";
  }
  const validationError = validateContract(contract);
  if (validationError) {
    return validationError;
  }

  return (
    <Accordion>
      <TransferForm
        onSubmit={transfer}
        state={operationState[operations.TRANSFER]}
        onChange={() => clearForm(operations.TRANSFER)}
      />
      <ApproveForm
        onChange={() => clearForm(operations.APPROVE)}
        onSubmit={approve}
        state={operationState[operations.APPROVE]}
      />
      <StorageTab storage={storage} />
    </Accordion>
  );

  function transfer({ fromAddress, toAddress, value }) {
    callMethod(contract, operations.TRANSFER, (methods) =>
      methods.transfer(fromAddress, toAddress, value).send()
    );
  }

  function approve({ spender, value }) {
    callMethod(contract, operations.APPROVE, (methods) =>
      methods.approve(spender, value).send()
    );
  }

  function clearForm(operationType) {
    dispatch({ type: types.CLEAR, operation: operationType });
  }

  async function callMethod(contract, operation, cb) {
    if (!contract) {
      return;
    }
    dispatch({ type: types.OPERATION_STARTED, operation });
    try {
      const op = await cb(contract.methods);
      await op.confirmation();
      contract.increaseOperationsCount();
      dispatch({ type: types.OPERATION_FINISHED, operation });
    } catch (error) {
      let errorMessage;
      if (error instanceof TezosOperationError) {
        switch (error.message) {
          case "NotEnoughAllowance":
            errorMessage = "Not Enough Allowance";
            break;
          case "NotEnoughBalance":
            errorMessage = "Not Enough Balance";
            break;
          default:
            errorMessage = error.message;
            break;
        }
      } else {
        errorMessage = "Unknown Error";
      }
      dispatch({
        type: types.OPERATION_FAILED,
        operation,
        error: errorMessage,
      });
      throw error;
    }
  }

  async function fetchAndSetStorage(address) {
    const storage = await fetchStorage(address);
    setStorage(storage);
  }
}

function stateReducer(state, action) {
  return {
    ...state,
    [action.operation]: operationReducer(state[action.operation], action),
  };
}

function operationReducer(state, action) {
  switch (action.type) {
    case types.OPERATION_STARTED:
      return { ...state, loading: true, error: "" };
    case types.OPERATION_FINISHED:
      return { ...state, loading: false, success: true, error: "" };
    case types.OPERATION_FAILED:
      return { ...state, loading: false, error: action.error };
    case types.CLEAR:
      return { ...state, error: null, loading: false, success: false };
    default:
      return state;
  }
}

function validateContract(contract) {
  const { methods } = contract;

  const missing = [];
  if (!methods.transfer) {
    missing.push("transfer");
  }

  if (!methods.approve) {
    missing.push("approve");
  }

  if (!methods.getAllowance) {
    missing.push("getAllowance");
  }

  if (!methods.getBalance) {
    missing.push("getBalance");
  }

  if (!methods.getTotalSupply) {
    missing.push("getTotalSupply");
  }

  if (missing.length) {
    console.log({ methods });
    return (
      "Contract doesn't look like a TZIP-7 FA1.2 contract. Missing: " +
      missing.join(", ")
    );
  }
}

async function fetchStorage(contractAddress) {
  const response = await fetch(
    `https://api.better-call.dev/v1/contract/carthagenet/${contractAddress}/storage?`
  );
  return await response.json();
}
