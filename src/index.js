import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { TezosToolkit } from "@taquito/taquito";
import { TezosProvider } from "@tezos-il/tezos-hooks";
import { Grommet } from "grommet";
const tezos = new TezosToolkit();

tezos.setProvider({
  rpc:
    "https://carthagenet.tezoslink.io/v1/7fc0ab41-2512-4947-a353-2c698104e74e",
});

ReactDOM.render(
  <React.StrictMode>
    <Grommet plain>
      <TezosProvider value={{ tezos }}>
        <App />
      </TezosProvider>
    </Grommet>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
