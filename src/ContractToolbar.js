import React, { useState } from "react";
import { Form, TextInput, Box, Button } from "grommet";
export function ContractToolbar({ address, onSubmit }) {
  const [contractAddress, setContractAddress] = useState(address);
  return (
    <Form
      className="contract-toolbar"
      onSubmit={() => onSubmit(contractAddress)}
    >
      <Box direction="row" gap="medium">
        <TextInput
          id="contractInput"
          value={contractAddress}
          placeholder="Contract"
          onChange={({ target: { value } }) => setContractAddress(value)}
        />

        <Box direction="row" gap="medium">
          <Button type="submit" primary label="Submit" />
        </Box>
      </Box>
    </Form>
  );
}
