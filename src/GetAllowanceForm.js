import React, { useReducer } from "react";
import {
  AccordionPanel,
  Box,
  Form,
  FormField,
  TextInput,
  Button,
} from "grommet";

import { OperationState } from "./OperationState";

const ACTION_TYPES = {
  SET_OWNER: "SET_OWNER",
  SET_SPENDER: "SET_SPENDER",
  CLEAR: "CLEAR",
};

export function GetAllowanceForm({ onSubmit, state, onChange }) {
  const [formState, dispatch] = useReducer(reducer, {
    owner: "",
  });
  return (
    <AccordionPanel label="Get Allowance">
      <Form onSubmit={submit} onReset={clear} disabled={state.loading}>
        <Box pad="medium" border direction="row" gap="medium" wrap>
          <FormField label="Owner" width="35%">
            <TextInput
              id="ownerInput"
              className="form-control"
              value={formState.owner}
              onChange={({ target: { value } }) => setOwner(value)}
            />
          </FormField>

          <FormField label="Spender" width="35%">
            <TextInput
              id="spenderInput"
              className="form-control"
              value={formState.spender}
              onChange={({ target: { value } }) => setSpender(value)}
            />
          </FormField>
          <Box
            direction="row"
            gap="medium"
            width="100%"
            justify="end"
            align="center"
          >
            <Button type="submit" primary label="Submit" />
            <Button type="reset" primary label="Clear" />
            <OperationState {...state} />
          </Box>
        </Box>
      </Form>
    </AccordionPanel>
  );

  function setOwner(value) {
    dispatch({ type: ACTION_TYPES.SET_OWNER, value });
    onChange();
  }

  function setSpender(value) {
    dispatch({ type: ACTION_TYPES.SET_SPENDER, value });
    onChange();
  }

  function clear() {
    dispatch({ type: ACTION_TYPES.CLEAR });
  }

  function submit() {
    onSubmit(state);
    clear();
  }
}

function reducer(state, action) {
  switch (action.type) {
    case ACTION_TYPES.SET_OWNER:
      return { ...state, owner: action.value };
    case ACTION_TYPES.SET_SPENDER:
      return { ...state, spender: action.value };
    case ACTION_TYPES.CLEAR:
      return { owner: "", spender: "" };
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
}
