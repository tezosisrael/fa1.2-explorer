import React, { useState, useEffect } from "react";
import { Box } from "grommet";
import { useWallet } from "@tezos-il/tezos-hooks";
import { ContractToolbar } from "./ContractToolbar";
import { ContractInterface } from "./ContractInterface";

function App() {
  const [contractAddress, setContractAddress] = useState(
    process.env.REACT_APP_DEFAULT_CONTRACT
  );
  const { connect: connectToWallet } = useWallet();

  useEffect(() => {
    connectToWallet();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Box className="App" pad="small">
      <ContractToolbar address={contractAddress} onSubmit={onSubmitContract} />
      <div className="contractBrowser">
        <ContractInterface address={contractAddress} />
      </div>
    </Box>
  );

  function onSubmitContract(contractAddress) {
    setContractAddress(contractAddress);
  }
}

export default App;
