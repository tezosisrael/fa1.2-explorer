import React from "react";
import { AccordionPanel, Box, Heading } from "grommet";

import { OperationState } from "./OperationState";

export function GetTotalSupplyForm({ onSubmit, state }) {
  return (
    <AccordionPanel
      label={
        <Box onClick={onSubmit} pad={{ horizontal: "xsmall" }} width="100%">
          <Heading level={4}>Get Total Supply</Heading>
        </Box>
      }
    >
      <Box height={{ min: "small" }}>
        <OperationState state={state} />
      </Box>
    </AccordionPanel>
  );
}
