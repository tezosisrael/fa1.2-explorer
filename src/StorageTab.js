import React from "react";
import { AccordionPanel, Box } from "grommet";

export function StorageTab({ storage }) {
  return (
    <AccordionPanel label="Storage">
      <Box height={{ min: "small" }}>{JSON.stringify(storage)}</Box>
    </AccordionPanel>
  );
}
