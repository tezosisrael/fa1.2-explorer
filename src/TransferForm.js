import React, { useReducer } from "react";
import {
  AccordionPanel,
  Box,
  Form,
  FormField,
  TextInput,
  Button,
} from "grommet";
import { OperationState } from "./OperationState";

const ACTION_TYPES = {
  SET_FROM_ADDRESS: "SET_FROM_ADDRESS",
  SET_TO_ADDRESS: "SET_TO_ADDRESS",
  SET_VALUE: "SET_VALUE",
  CLEAR: "CLEAR",
};

export function TransferForm({ onSubmit, state: operationState, onChange }) {
  const [formState, dispatch] = useReducer(reducer, {
    fromAddress: process.env.REACT_APP_DEFAULT_SPENDER,
    toAddress: process.env.REACT_APP_DEFAULT_OWNER,
    value: 15,
  });
  return (
    <AccordionPanel label="Transfer">
      <Form onSubmit={submit} onReset={clear} disabled={operationState.loading}>
        <Box pad="medium" border direction="row" gap="medium" wrap>
          <FormField label="From" width="35%">
            <TextInput
              id="fromInput"
              className="form-control"
              value={formState.fromAddress}
              onChange={({ target: { value } }) => setFromAddress(value)}
            />
          </FormField>
          <FormField label="To" width="35%">
            <TextInput
              id="toInput"
              className="form-control"
              value={formState.toAddress}
              onChange={({ target: { value } }) => setToAddress(value)}
            />
          </FormField>
          <FormField label="Value" width="25%">
            <TextInput
              id="valueInput"
              className="form-control"
              value={formState.value}
              onChange={({ target: { value } }) => setValue(value)}
            />
          </FormField>

          <Box
            direction="row"
            gap="medium"
            width="100%"
            justify="end"
            align="center"
          >
            <Button type="submit" primary label="Submit" />
            <Button type="reset" primary label="Clear" />
            <OperationState {...operationState} />
          </Box>
        </Box>
      </Form>
    </AccordionPanel>
  );

  function setFromAddress(value) {
    onChange();
    dispatch({ type: ACTION_TYPES.SET_FROM_ADDRESS, value });
  }

  function setToAddress(value) {
    onChange();
    dispatch({ type: ACTION_TYPES.SET_TO_ADDRESS, value });
  }

  function setValue(value) {
    onChange();
    dispatch({ type: ACTION_TYPES.SET_VALUE, value: Number(value) });
  }

  function clear() {
    dispatch({ type: ACTION_TYPES.CLEAR });
  }

  async function submit() {
    try {
      await onSubmit(formState);
    } catch (e) {
      console.error(e);
    }
  }
}

function reducer(state, action) {
  switch (action.type) {
    case ACTION_TYPES.SET_FROM_ADDRESS:
      return { ...state, fromAddress: action.value };
    case ACTION_TYPES.SET_TO_ADDRESS:
      return { ...state, toAddress: action.value };
    case ACTION_TYPES.SET_VALUE:
      return { ...state, value: parseInt(action.value, 10) };
    case ACTION_TYPES.CLEAR:
      return { fromAddress: "", toAddress: "", value: 0 };
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
}
