import React, { useReducer } from "react";
import {
  AccordionPanel,
  Box,
  Form,
  FormField,
  TextInput,
  Button,
} from "grommet";

import { OperationState } from "./OperationState";

const ACTION_TYPES = {
  SET_OWNER: "SET_OWNER",
  CLEAR: "CLEAR",
};

export function GetBalanceForm({ onSubmit, state, onChange }) {
  const [formState, dispatch] = useReducer(reducer, {
    owner: "",
  });
  return (
    <AccordionPanel label="Get Balance">
      <Form onSubmit={submit} onReset={clear} disabled={state.loading}>
        <Box pad="medium" border direction="row" gap="medium" wrap>
          <FormField label="Owner" width="35%">
            <TextInput
              id="ownerInput"
              className="form-control"
              value={formState.owner}
              onChange={({ target: { value } }) => setOwner(value)}
            />
          </FormField>

          <Box direction="row" gap="medium" width="100%" justify="end">
            <Button type="submit" primary label="Submit" />
            <Button type="reset" primary label="Clear" />
            <OperationState {...state} />
          </Box>
        </Box>
      </Form>
    </AccordionPanel>
  );

  function setOwner(value) {
    dispatch({ type: ACTION_TYPES.SET_OWNER, value });
    onChange();
  }

  function clear() {
    dispatch({ type: ACTION_TYPES.CLEAR });
  }

  function submit() {
    onSubmit(formState);
    clear();
  }
}

function reducer(state, action) {
  switch (action.type) {
    case ACTION_TYPES.SET_OWNER:
      return { ...state, owner: action.value };
    case ACTION_TYPES.CLEAR:
      return { owner: "" };
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
}
