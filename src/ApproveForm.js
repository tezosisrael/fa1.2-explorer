import React, { useReducer } from "react";
import {
  AccordionPanel,
  Box,
  Form,
  FormField,
  TextInput,
  Button,
} from "grommet";

import { OperationState } from "./OperationState";

const ACTION_TYPES = {
  SET_SPENDER: "SET_SPENDER",
  SET_VALUE: "SET_VALUE",
  CLEAR: "CLEAR",
};

export function ApproveForm({ onSubmit, onChange, state }) {
  const [formState, dispatch] = useReducer(reducer, {
    spender: "",
    value: 0,
  });
  return (
    <AccordionPanel label="Approve">
      <Form onSubmit={submit} onReset={clear} disabled={state.loading}>
        <Box pad="medium" border direction="row" gap="medium" wrap>
          <FormField label="Spender" width="35%">
            <TextInput
              id="spenderInput"
              className="form-control"
              value={formState.spender}
              onChange={({ target: { value } }) => setSpender(value)}
            />
          </FormField>
          <FormField label="Value" width="35%">
            <TextInput
              id="valueInput"
              className="form-control"
              value={formState.value}
              onChange={({ target: { value } }) => setValue(value)}
            />
          </FormField>

          <Box
            direction="row"
            gap="medium"
            width="100%"
            justify="end"
            align="center"
          >
            <Button type="submit" primary label="Submit" />
            <Button type="reset" primary label="Clear" />
            <OperationState {...state} />
          </Box>
        </Box>
      </Form>
    </AccordionPanel>
  );

  function setSpender(value) {
    dispatch({ type: ACTION_TYPES.SET_SPENDER, value });
    onChange();
  }

  function setValue(value) {
    dispatch({ type: ACTION_TYPES.SET_VALUE, value: Number(value) });
    onChange();
  }

  function clear() {
    dispatch({ type: ACTION_TYPES.CLEAR });
  }

  function submit() {
    onSubmit(formState);
    clear();
  }
}

function reducer(state, action) {
  switch (action.type) {
    case ACTION_TYPES.SET_SPENDER:
      return { ...state, spender: action.value };
    case ACTION_TYPES.SET_VALUE:
      return { ...state, value: parseInt(action.value, 10) };
    case ACTION_TYPES.CLEAR:
      return { spender: "", value: 0 };
    default:
      throw new Error(`Unhandled type: ${action.type}`);
  }
}
